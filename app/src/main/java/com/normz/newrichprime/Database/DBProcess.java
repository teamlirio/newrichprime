package com.normz.newrichprime.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.util.Log;

import com.normz.newrichprime.Beans.Product;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

/**
 * Created by fluxion inc on 29/01/2018.
 */

public class DBProcess extends DBHandler {
    private Context mContext;
    private SQLiteDatabase db;
    private Cursor res;
    private String sql = "";
    public DBProcess(Context context) {
        super(context);
    }


    public String test() {
        return "success";
    }

    public boolean saveDetails(Product product) {
        db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("Name", product.getName());
        cv.put("Quantity", product.getQty());
        cv.put("Price", product.getPrice());
        cv.put("OffTake", product.getPrice());
        cv.put("Planograms", product.isPlanograms());
        cv.put("StoreID", product.getStoreid());
        boolean insertSuccess = db.insert("Products", null, cv) > 0;
        if(insertSuccess) {
            Log.v("INSERT TEXT DETAILS", "SUCCESS");
        } else {
            Log.v("INSERT TEXT DETAILS", "FAILED");
        }
        if(product.isPlanograms()) {
            saveImages(product.getImage1(), product.getImage2(), product.getStoreid());
        }
        return insertSuccess;
    }

    private void saveImages(Bitmap image1, Bitmap image2, int storeid) {
        db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("Image1", getBytes(image1));
        cv.put("Image2", getBytes(image2));
        cv.put("StoreID", storeid);
        boolean insertSuccess = db.insert("Images", null, cv) > 0;
        if(insertSuccess) {
            Log.v("INSERT IMAGE DETAILS", "SUCCESS");
        } else {
            Log.v("INSERT IMAGE DETAILS", "FAILED");
        }
    }

    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if(bitmap != null ){
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        }
        return stream.toByteArray();

    }

    public void getSavedDetails() {

    }
}
