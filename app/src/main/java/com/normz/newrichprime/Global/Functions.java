package com.normz.newrichprime.Global;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;


import com.normz.newrichprime.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fluxion inc on 11/07/2017.
 */

public class Functions {


    public static void nextFragment(Activity activity, Fragment nextFrag) {
       FragmentTransaction ft = ((AppCompatActivity)activity).getSupportFragmentManager().beginTransaction();
       ft.replace(R.id.fragment_container, nextFrag).addToBackStack(null);
       ft.commit();
   }


    public static void showErrorMessage(Activity activity, String message, String title) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setMessage(message);
        alert.setTitle(title);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog a = alert.create();
        a.show();
    }


    public static List<JSONObject> convertToList(JSONArray jsonArray) {
        List<JSONObject> list = new ArrayList<JSONObject>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                list.add(jsonArray.getJSONObject(i));
            } catch (JSONException e) {
                System.out.println("Unable to convert to list");
                e.printStackTrace();
            }
        }
        return list;
    }


    public static Bitmap getPhoto(Intent data) {
        Bitmap photo = (Bitmap) data.getExtras().get("data");
        photo = Bitmap.createScaledBitmap(photo, 100, 100, false);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
        return photo;
    }



}
