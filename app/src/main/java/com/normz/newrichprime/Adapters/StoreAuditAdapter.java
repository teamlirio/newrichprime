package com.normz.newrichprime.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.normz.newrichprime.Beans.Item;
import com.normz.newrichprime.Beans.Product;
import com.normz.newrichprime.Global.Variables;
import com.normz.newrichprime.R;

import java.util.ArrayList;

/**
 * Created by fluxion inc on 27/11/2017.
 */

public class StoreAuditAdapter extends RecyclerView.Adapter<StoreAuditAdapter.SAViewHolder> {
    private Context mContext;
    private ArrayList<Item> list;
    private View rootView;
    private int stat, pos;
    public OpenItemActivity openItemActivity;

    public interface OpenItemActivity {
        void openAct(View v, int position, String product);
    }

    public StoreAuditAdapter(Context context, ArrayList<Item> list) {
        this.mContext = context;
        this.list = list;
    }

    public void setOpenMethod(OpenItemActivity listener) {
        this.openItemActivity = listener;
    }

    @Override
    public SAViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_storeaudit, parent, false);

        return new SAViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final SAViewHolder holder, final int position) {
        holder.tvName.setText(list.get(position).getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String p = list.get(position).getName();
                Variables.productIndexSelected = position;
                openItemActivity.openAct(v, position, p);
            }
        });
        Log.v("BGCOLOR", "Status : " + list.get(pos).getStatus() + " -- Stat : " + stat);
        Log.v("BGCOLOR", "Position : " + list.get(pos).getPosition() + " --  Pos:" + pos + " -- P : " + position);
        if (Variables.productIndexSelected == position) {
            holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.md_green_400));
        }

    }

    public void clearData() {

    }
    public void changeColor(int status, int pos) {

        Item item = list.get(pos);
        item.setPosition(pos);
        item.setStatus(status);
        Log.v("StoreAuditAdapte", "Status : " + status + ", Pos : " + pos + " -- " + list.get(pos).getPosition() + " S: " + list.get(pos).getStatus());
        this.stat = status;
        this.pos = pos;
        notifyItemChanged(pos);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SAViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;

        public SAViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_storeaudit_header);
        }
    }
}
