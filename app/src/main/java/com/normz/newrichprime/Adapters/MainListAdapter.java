package com.normz.newrichprime.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.normz.newrichprime.Beans.Branch;
import com.normz.newrichprime.R;

import java.util.ArrayList;

/**
 * Created by fluxion inc on 21/11/2017.
 */

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.MainListViewHolder> {
    private Context mContext;
    private ArrayList<Branch> branches;
    private View rootView;
    public OpenActivity openActivity;
    public interface OpenActivity {
        void openAct(View v, int position, ArrayList<Branch> list, int storeId);
    }
    public void setOpenMethod(OpenActivity listener) {
        this.openActivity = listener;
    }
    public MainListAdapter(Context context, ArrayList<Branch> list) {
        this.mContext = context;
        this.branches = list;
    }
    @Override
    public MainListAdapter.MainListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard, parent, false);
        return new MainListViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(MainListAdapter.MainListViewHolder holder, final int position) {
        holder.tvBranch.setText(branches.get(position).getStorename());
        holder.tvStreet.setText(branches.get(position).getStoreadd());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               openActivity.openAct(v, position, branches,branches.get(position).getStoreid());
            }
        });

    }

    @Override
    public int getItemCount() {
        return branches.size();
    }

    public class MainListViewHolder extends RecyclerView.ViewHolder{
        TextView tvBranch, tvStreet;
        ImageView ivMark;
        public MainListViewHolder(View itemView) {
            super(itemView);
            tvBranch =  itemView.findViewById(R.id.tvItmDBBranchName);
            tvStreet =  itemView.findViewById(R.id.tvItmDBStreetName);
        }
    }
}
