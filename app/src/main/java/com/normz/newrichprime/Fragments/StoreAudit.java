package com.normz.newrichprime.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.normz.newrichprime.Activity.ContainerActivity;
import com.normz.newrichprime.Activity.ProductActivity;
import com.normz.newrichprime.Adapters.StoreAuditAdapter;
import com.normz.newrichprime.Beans.Item;
import com.normz.newrichprime.R;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StoreAudit.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StoreAudit#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StoreAudit extends Fragment implements StoreAuditAdapter.OpenItemActivity {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int PRODUCT_ACTIVITY = 200;
    private RecyclerView rvItems;
    private Button btnAddPhoto, btnNext;
    private Switch swNewDeliveries;
    private EditText etPriceFeedback;
    private LinearLayout llPhotoContainer;
    private int index, storeid;
    private  StoreAuditAdapter saAdapter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public StoreAudit() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StoreAudit.
     */
    // TODO: Rename and change types and number of parameters
    public static StoreAudit newInstance(String param1, String param2) {
        StoreAudit fragment = new StoreAudit();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_store_audit, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((ContainerActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ContainerActivity) getActivity()).getSupportActionBar().setTitle("Store Audit");
        rvItems = view.findViewById(R.id.rvITem_StoreAudit);
        btnAddPhoto = view.findViewById(R.id.btn_storeaudit_addphoto);
        llPhotoContainer = view.findViewById(R.id.llStoreAudit_photocontainer);
        btnAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            }
        });
        rvItems.setLayoutManager(new LinearLayoutManager(getActivity()));
        saAdapter = new StoreAuditAdapter(getActivity(), filler());
        rvItems.setAdapter(saAdapter);
        saAdapter.setOpenMethod(this);
        Bundle bundle = this.getArguments();
        if(bundle != null) {
            storeid = bundle.getInt("StoreID");
            index = bundle.getInt("Index");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if(resultCode == RESULT_OK) {

            }
        }

        if(requestCode == PRODUCT_ACTIVITY) {
            if(resultCode == RESULT_OK) {
                int position = data.getExtras().getInt("pIndex");
                int status = data.getExtras().getInt("Status");

                Log.v("StoreAudit", position + " -- " + " -- " + status + " -- " +position);
                if(status == 1 ) {
                    saAdapter.changeColor(status, position);
                }
            }
        }
    }

    public ArrayList<Item> filler() {
        ArrayList<Item> list = new ArrayList<>();
        list.add(new Item("Hot Wheels", 0, 0));
        list.add(new Item("Shopkins 2s", 0, 1));
        list.add(new Item("Uno Cards", 0, 2));
        list.add(new Item("Disney Placemats", 0, 3));
        list.add(new Item("Scrabble", 0, 4));
        list.add(new Item("Finding Dory", 0,5));

        return list;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private Bundle storeIndex() {
        Bundle bundle = new Bundle();
        bundle.putInt("StoreID", storeid);
        bundle.putInt("Index", index);
        return bundle;
    }

    @Override
    public void openAct(View v, int pos, String product) {
        switch(pos) {
            case 0 :
                startProductActivity(product, 0);
                break;
            case 1 :
                startProductActivity(product, 1);
                break;
            case 2 :
                startProductActivity(product, 2);
                break;
            case 3 :
                startProductActivity(product, 3);
                break;
            case 4 :
                startProductActivity(product, 4);
                break;
            case 5 :
                startProductActivity(product, 5);
                break;
        }

    }

    public void startProductActivity(String product, int index) {
        Intent intent = new Intent(getActivity(), ProductActivity.class);
        intent.putExtra("ProductName", product);
        intent.putExtra("IndexOfProduct", index);
        intent.putExtras(storeIndex());
        startActivityForResult(intent, PRODUCT_ACTIVITY);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
