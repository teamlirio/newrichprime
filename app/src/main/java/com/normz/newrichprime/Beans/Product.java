package com.normz.newrichprime.Beans;

import android.graphics.Bitmap;

/**
 * Created by fluxion inc on 27/11/2017.
 */

public class Product {
    private String name;
    private int storeid;
    private int qty;
    private int price;
    private int offtake;
    private boolean planograms;
    private Bitmap image1;
    private Bitmap image2;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStoreid() {
        return storeid;
    }

    public void setStoreid(int storeid) {
        this.storeid = storeid;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getOfftake() {
        return offtake;
    }

    public void setOfftake(int offtake) {
        this.offtake = offtake;
    }

    public boolean isPlanograms() {
        return planograms;
    }

    public void setPlanograms(boolean planograms) {
        this.planograms = planograms;
    }

    public Bitmap getImage1() {
        return image1;
    }

    public void setImage1(Bitmap image1) {
        this.image1 = image1;
    }

    public Bitmap getImage2() {
        return image2;
    }

    public void setImage2(Bitmap image2) {
        this.image2 = image2;
    }
}
