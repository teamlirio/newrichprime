package com.normz.newrichprime.Beans;

/**
 * Created by fluxion inc on 07/02/2018.
 */

public class Item {
    private String name;
    private int status;
    private int position;

    public Item() {
    }

    public Item(String name, int status, int position) {
        this.name = name;
        this.status = status;
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Item(String name, int status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
