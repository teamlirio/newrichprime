package com.normz.newrichprime.Beans;

import java.io.Serializable;

/**
 * Created by fluxion inc on 13/07/2017.
 */

public class Branch implements Serializable {
    private int storeid;
    private String storename;
    private int storecode;
    private String storeadd;
    private double latitude;
    private double longitude;
    private String clustertype;
    private String contactperson;
    private String conctactjobtitle;
    private String contactnum;
    private String dob;
    private int acctid;
    private boolean storeflag;

    public Branch() {
    }

    public Branch(int storeid, String storename, int storecode, String storeadd, double latitude, double longitude, String clustertype, String contactperson, String conctactjobtitle, String contactnum, String dob, int acctid, boolean storeflag) {
        this.storeid = storeid;
        this.storename = storename;
        this.storecode = storecode;
        this.storeadd = storeadd;
        this.latitude = latitude;
        this.longitude = longitude;
        this.clustertype = clustertype;
        this.contactperson = contactperson;
        this.conctactjobtitle = conctactjobtitle;
        this.contactnum = contactnum;
        this.dob = dob;
        this.acctid = acctid;
        this.storeflag = storeflag;
    }

    public int getStoreid() {
        return storeid;
    }

    public void setStoreid(int storeid) {
        this.storeid = storeid;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public int getStorecode() {
        return storecode;
    }

    public void setStorecode(int storecode) {
        this.storecode = storecode;
    }

    public String getStoreadd() {
        return storeadd;
    }

    public void setStoreadd(String storeadd) {
        this.storeadd = storeadd;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getClustertype() {
        return clustertype;
    }

    public void setClustertype(String clustertype) {
        this.clustertype = clustertype;
    }

    public String getContactperson() {
        return contactperson;
    }

    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    public String getConctactjobtitle() {
        return conctactjobtitle;
    }

    public void setConctactjobtitle(String conctactjobtitle) {
        this.conctactjobtitle = conctactjobtitle;
    }

    public String getContactnum() {
        return contactnum;
    }

    public void setContactnum(String contactnum) {
        this.contactnum = contactnum;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getAcctid() {
        return acctid;
    }

    public void setAcctid(int acctid) {
        this.acctid = acctid;
    }

    public boolean isStoreflag() {
        return storeflag;
    }

    public void setStoreflag(boolean storeflag) {
        this.storeflag = storeflag;
    }
}
