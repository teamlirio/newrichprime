package com.normz.newrichprime.Activity;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.normz.newrichprime.Beans.Branch;
import com.normz.newrichprime.Fragments.Details;
import com.normz.newrichprime.Fragments.StoreAudit;
import com.normz.newrichprime.R;

import java.util.ArrayList;

public class ContainerActivity extends AppCompatActivity implements Details.OnFragmentInteractionListener,
        StoreAudit.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        int idx = getIntent().getExtras().getInt("Position");
        int storeid = getIntent().getExtras().getInt("StoreID");
        ArrayList<Branch> list = (ArrayList<Branch>) getIntent().getExtras().getSerializable("Branches");
        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            Fragment frag = new Details();
         //   Log.v("CONTAINER ACT", "");
            bundle.putInt("Index", idx);
            bundle.putInt("StoreId", storeid);
            bundle.putSerializable("Details", list);
            frag.setArguments(bundle);
            ft.replace(R.id.fragment_container, frag);
            ft.commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                //  Toast.makeText(ContainerActivity.this, "Test Back button", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
