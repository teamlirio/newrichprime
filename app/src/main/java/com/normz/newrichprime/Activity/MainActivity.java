package com.normz.newrichprime.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.normz.newrichprime.Adapters.MainListAdapter;
import com.normz.newrichprime.Beans.Branch;
import com.normz.newrichprime.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainListAdapter.OpenActivity {
    private RecyclerView rvItemDashBoard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        rvItemDashBoard = findViewById(R.id.rvItmDashboard);
        rvItemDashBoard.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        MainListAdapter adapter = new MainListAdapter(MainActivity.this, filler());
        rvItemDashBoard.setAdapter(adapter);
        adapter.setOpenMethod(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public ArrayList<Branch> filler() {
        ArrayList list = new ArrayList<>();
        list.add(new Branch(99,"store1",1,"Makati",0.0,0.0,"Residential", "Manong Boy", "President", "0912323123", "12/1/12", 0101, false));
        list.add(new Branch(98,"store2",2,"Malabon",0.1,0.2,"Commercial", "Manong Badong", "CEO", "0912323124", "12/2/12", 0102, false));
        list.add(new Branch(97,"store3",3,"Taguig",0.3,0.4,"Residential", "Manong Pedro", "General", "0912323125", "12/3/12", 0103, false));
        list.add(new Branch(96,"store4",4,"Navotas",0.5,0.6,"Commercial", "Manong Pablo", "GOAT", "0912323126", "12/4/12", 0104, false));
        return list;
    }

    @Override
    public void openAct(View v, int position, ArrayList<Branch> list, int storeId) {
        Intent intent = new Intent(MainActivity.this, ContainerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("StoreID", storeId);
        bundle.putInt("Position", position);
        Log.v("MA", storeId + "  " + position);
        bundle.putSerializable("Branches", list);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
