package com.normz.newrichprime.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TableRow;
import android.widget.Toast;

import com.normz.newrichprime.Beans.Product;
import com.normz.newrichprime.Database.DBProcess;
import com.normz.newrichprime.Global.Functions;
import com.normz.newrichprime.R;

public class ProductActivity extends AppCompatActivity {
    private Button btnAddPhoto, btnCancel, btnSave;
    private Switch swPlanograms;
    private EditText etQty, etPrice, etOfftake;
    private LinearLayout llcontainer;
    private TableRow trContainer;
    private int CAMERA_REQUEST_ADDPHOTO = 100;
    private int EDIT_PHOTO_1 = 1;
    private int EDIT_PHOTO_2 = 2;
    boolean isFull, isOne, isTwo;
    private ImageView iv1, iv2;
    private int storeid, index;
    private String productName;
    private int productIndex;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String p = getIntent().getExtras().getString("ProductName");
        productIndex = getIntent().getExtras().getInt("IndexOfProduct");
        Log.v("posidx", productIndex+"");
        Bundle bundle = getIntent().getExtras();
        switch(p) {
            case "Hot Wheels" :
                getSupportActionBar().setTitle("Hot Wheels");
                productName = "Hot Wheels";
                break;
            case "Shopkins 2s" :
                getSupportActionBar().setTitle("Shopkins 2s");
                productName = "Shopkins 2s";
                break;
            case "Uno Cards" :
                getSupportActionBar().setTitle("Uno Cards");
                productName = "Uno Cards";
                break;
            case "Disney Placemats" :
                getSupportActionBar().setTitle("Disney Placemats");
                productName = "Disney Placemats";
                break;
            case "Scrabble" :
                getSupportActionBar().setTitle("Scrabble");
                productName = "Scrabble";
                break;
            case "Finding Dory" :
                getSupportActionBar().setTitle("Finding Dory");
                productName = "Finding Dory";
                break;
        }
        String testConnection = new DBProcess(ProductActivity.this).test();
       // Log.v("DB CONNECTION", testConnection);
        storeid = bundle.getInt("StoreID");
        index = bundle.getInt("Index");
        Log.v("PA", "Store ID : " + storeid + ", Index : " + index);
        setContentView(R.layout.activity_products);
        btnAddPhoto = findViewById(R.id.btnHW_storeaudit_addphoto);
        swPlanograms = findViewById(R.id.swHW_storeaudit_plano);
        etQty = findViewById(R.id.etHW_qty);
        etPrice = findViewById(R.id.etHW_price);
        etOfftake = findViewById(R.id.etHW_offtake);
        trContainer = findViewById(R.id.tbHWitem_storeaudit_container);
        btnCancel = findViewById(R.id.btnHW_cancel);
        iv1 = findViewById(R.id.ivHWitem_storeaudit_1);
        iv2 = findViewById(R.id.ivHWitem_storeaudit_2);
        btnSave = findViewById(R.id.btnSave);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        llcontainer = findViewById(R.id.llHW_photocontainer);


        swPlanograms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    llcontainer.setVisibility(View.VISIBLE);
                } else {
                    llcontainer.setVisibility(View.GONE);
                }
            }
        });

        btnAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST_ADDPHOTO);
                isFull = false;
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBProcess dbProcess = new DBProcess(ProductActivity.this);
                if(dbProcess.saveDetails(processSaveButton())) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("pIndex", productIndex);
                    returnIntent.putExtra("Status", 1);
                    setResult(RESULT_OK, returnIntent);
                    finish();

                } else {
                    Toast.makeText(ProductActivity.this, "Something went wrong.", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    public Product processSaveButton() {
        Product product = new Product();
        product.setName(productName);
        product.setStoreid(storeid);
        if(etQty.getText().toString().matches("")){
            product.setQty(0);
        } else {
            product.setQty(Integer.parseInt(etQty.getText().toString()));
        }

        if(etPrice.getText().toString().matches("")){
            product.setPrice(0);
        } else {
            product.setPrice(Integer.parseInt(etPrice.getText().toString()));
        }
        if(etOfftake.getText().toString().matches("")){
            product.setOfftake(0);
        } else {
            product.setOfftake(Integer.parseInt(etOfftake.getText().toString()));
        }

        if(swPlanograms.isChecked()) {
            if(iv1.getDrawable() != null) {
                product.setImage1(((BitmapDrawable)iv1.getDrawable()).getBitmap());
            } else {
                product.setImage1(null);
            }
            if(iv2.getDrawable() != null) {
                product.setImage2(((BitmapDrawable)iv2.getDrawable()).getBitmap());
            } else {
                product.setImage2(null);
            }

        }
        return product;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CAMERA_REQUEST_ADDPHOTO) {
            if(resultCode == RESULT_OK) {
                    Bitmap photo = Functions.getPhoto(data);
                    if(!isFull) {
                        if(iv1.getDrawable() == null) {
                            iv1.setImageBitmap(photo);
                            iv1.setVisibility(View.VISIBLE);
                            isOne = true;
                        } else if (iv2.getDrawable() == null) {
                            iv2.setVisibility(View.VISIBLE);
                            iv2.setImageBitmap(photo);
                            isTwo = true;
                            isFull = true;
                            btnAddPhoto.setVisibility(View.GONE);
                        }
                    } else {

                        if(isOne) {
                            iv1.setImageBitmap(photo);
                        }

                        if(isTwo) {
                            iv2.setImageBitmap(photo);
                        }
                    }

                    if(iv1.getDrawable() != null) {
                        iv1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, CAMERA_REQUEST_ADDPHOTO);
                                isFull = true;
                                isOne = true;
                                isTwo = false;
                            }
                        });
                    }

                    if(iv2.getDrawable() != null) {
                        iv2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, CAMERA_REQUEST_ADDPHOTO);
                                isFull = true;
                                isTwo = true;
                                isOne = false;
                            }
                        });
                    }
            }

        }


    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                //  Toast.makeText(ContainerActivity.this, "Test Back button", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
